﻿using Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        blogdbEntities BlogEnt = new blogdbEntities();
        public ActionResult Index()
        {   
            
            HomepageDTO dto = new HomepageDTO();
            dto.Articles = BlogEnt.Articles.ToList();
            return View(dto.Articles);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            ViewBag.Message = "Login page.";

            return View();
        }

        [HttpPost]
        public ActionResult LoggedInMember()
        {
            ViewBag.Message = "LoginValidate page.";
            return View();
        }
        public ActionResult Article(int id, int authorID)
        {
            var coming = BlogEnt.Articles.Find(id);
            ViewBag.AuthorName = BlogEnt.Authors.Find(authorID).FirstName +" "+ BlogEnt.Authors.Find(authorID).LastName;
            return View(coming);
        }

    }
    public class HomepageDTO
    {
        public List<Admins> Admins { set; get; }
        public List<Articles> Articles { set; get; }
        public List<Authors> Authors { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Members> Members { get; set; }
    }
}