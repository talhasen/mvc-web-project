﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.AdminPanel.Controllers
{
    public class AdminHomeController : Controller
    {
        // GET: AdminPanel/AdminHome
        public ActionResult Index()
        {
            return View();
        }
    }
}